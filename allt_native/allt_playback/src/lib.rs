#![allow(clippy::new_without_default)]

pub mod c_api;
pub mod config;
pub mod error;
pub mod ipc;
pub mod player;

pub use cpal;
use std::path::PathBuf;
use tracing::level_filters::LevelFilter;
use tracing::{error, info, Level};
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::Layer;

pub fn init_logging() {
    // Log to stderr
    let subscriber = tracing_subscriber::Registry::default().with(
        tracing_subscriber::fmt::layer()
            .with_writer(std::io::stderr)
            .with_ansi(false)
            .with_filter(LevelFilter::from_level(Level::DEBUG)),
    );

    // let log_file = std::env::temp_dir().join(format!("allt_{}.log", std::process::id()));
    // let log_file = std::fs::File::create(log_file).expect("Failed to open log file");

    // let subscriber = subscriber.with(
    //     tracing_subscriber::fmt::layer()
    //         .with_writer(log_file)
    //         .with_ansi(false)
    //         .with_filter(LevelFilter::from_level(Level::DEBUG)),
    // );

    if let Err(e) = tracing::subscriber::set_global_default(subscriber) {
        error!("Failed to set logger (we already have a logger?) {e:?}");
    }
    info!("ALLT logging initialized");
}

fn current_dylib_path() -> Option<PathBuf> {
    #[cfg(any(target_os = "linux", target_os = "macos"))]
    unsafe {
        use std::ffi::{CStr, OsStr};
        use std::mem::MaybeUninit;
        use std::os::unix::ffi::OsStrExt;

        let mut dl_info = MaybeUninit::uninit();
        if libc::dladdr(
            current_dylib_path as *const libc::c_void,
            dl_info.as_mut_ptr(),
        ) != 0
        {
            let dl_info = dl_info.assume_init();
            let filepath = CStr::from_ptr(dl_info.dli_fname).to_bytes();
            let filepath = OsStr::from_bytes(filepath);
            Some(PathBuf::from(filepath))
        } else {
            None
        }
    }

    #[cfg(target_os = "windows")]
    unsafe {
        use std::ffi::OsString;
        use std::mem::{self, MaybeUninit};
        use std::os::windows::ffi::OsStringExt;
        use windows_sys::core::{PCWSTR, PWSTR};
        use windows_sys::Win32::Foundation::{GetLastError, ERROR_SUCCESS, MAX_PATH};
        use windows_sys::Win32::System::LibraryLoader::*;

        let mut module = MaybeUninit::uninit();
        let success = GetModuleHandleExW(
            GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
            current_dylib_path as PCWSTR,
            module.as_mut_ptr(),
        ) != 0;
        if success {
            let module = module.assume_init();
            let mut path_buf: [MaybeUninit<u16>; MAX_PATH as usize] =
                MaybeUninit::uninit().assume_init();
            let len = GetModuleFileNameW(module, path_buf.as_mut_ptr() as PWSTR, MAX_PATH);
            if len > 0 {
                if GetLastError() == ERROR_SUCCESS {
                    let path_buf = mem::transmute::<_, &[u16]>(&path_buf[..len as usize]);
                    let path_str = OsString::from_wide(path_buf);
                    Some(PathBuf::from(path_str))
                } else {
                    // TODO: Handle long filenames? (Check for ERROR_INSUFFICIENT_BUFFER)
                    None
                }
            } else {
                None
            }
        } else {
            None
        }
    }
}
