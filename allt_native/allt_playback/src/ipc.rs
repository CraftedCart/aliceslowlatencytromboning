//! IPC between the mod and the config process, over stdio

use crate::config::Config;
use byteorder::{NativeEndian, ReadBytesExt, WriteBytesExt};
use rtrb::{Consumer, PopError, Producer, PushError, RingBuffer};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::io;
use std::io::{Error, IsTerminal, Read, Stdin, Stdout, Write};
use std::marker::PhantomData;
use std::process::{ChildStdin, ChildStdout};
use thiserror::Error;

pub struct Endpoint<W: Write, R: Read + Send + 'static> {
    writer: W,
    reader: Consumer<io::Result<Vec<u8>>>,
    _reader_inner: PhantomData<R>,
}

pub type Server = Endpoint<ChildStdin, ChildStdout>;
pub type Client = Endpoint<Stdout, Stdin>;

/// Client-to-server message
#[derive(Serialize, Deserialize)]
pub enum IpcRequest {
    RequestConfig,
    SaveConfig(Config),
}

/// Server-to-client message
#[derive(Serialize, Deserialize)]
pub struct IpcResponseSendConfig(pub Config);

#[derive(Error, Debug)]
pub enum ClientConnectError {
    /// We shouldn't be IPC-ing with a terminal
    #[error("cannot do IPC with a terminal")]
    ConnectedToTerminal,
}

#[derive(Error, Debug)]
pub enum RecvMessageError {
    #[error("failed to receive data")]
    Io(io::Error),

    #[error("received data was malformed")]
    MalformedData,
}

impl<W: Write, R: Read + Send + 'static> Endpoint<W, R> {
    fn start_reader_thread(mut reader: R) -> Consumer<io::Result<Vec<u8>>> {
        let (mut tx, rx) = RingBuffer::new(64);

        std::thread::spawn(move || loop {
            let size = match reader.read_u16::<NativeEndian>() {
                Ok(size) => size,
                Err(e) => {
                    Self::push_to_rtrb(&mut tx, Err(e));
                    return;
                }
            };

            let mut buf = vec![0; size as _];
            match reader.read_exact(&mut buf[..]) {
                Ok(_) => {}
                Err(e) => {
                    Self::push_to_rtrb(&mut tx, Err(e));
                    return;
                }
            }

            Self::push_to_rtrb(&mut tx, Ok(buf));
        });

        rx
    }

    fn push_to_rtrb(tx: &mut Producer<Result<Vec<u8>, Error>>, mut message: io::Result<Vec<u8>>) {
        loop {
            match tx.push(message) {
                Ok(_) => {
                    break;
                }
                Err(PushError::Full(returned_message)) => {
                    message = returned_message;
                    std::thread::yield_now();
                }
            }
        }
    }

    pub fn from_streams(writer: W, reader: R) -> Self {
        Self {
            writer,
            reader: Self::start_reader_thread(reader),
            _reader_inner: PhantomData,
        }
    }

    pub fn send_bytes(&mut self, message: &[u8]) -> io::Result<()> {
        self.writer
            .write_u16::<NativeEndian>(message.len().try_into().expect("message too big"))?;
        self.writer.write_all(message)?;
        self.writer.flush()
    }

    pub fn recv_bytes(&mut self) -> io::Result<Vec<u8>> {
        loop {
            match self.reader.pop() {
                Ok(message) => break message,
                Err(PopError::Empty) => {
                    // Go again
                    std::thread::yield_now()
                }
            }
        }
    }

    /// Doesn't block if we don't have a message to receive
    pub fn recv_bytes_non_blocking(&mut self) -> Option<io::Result<Vec<u8>>> {
        self.reader.pop().ok()
    }

    pub fn send_message<T: Serialize>(&mut self, message: &T) -> io::Result<()> {
        self.send_bytes(&bincode::serialize(message).expect("serialization failed"))
    }

    pub fn recv_message<T: DeserializeOwned>(&mut self) -> Result<T, RecvMessageError> {
        let bytes = match self.recv_bytes() {
            Ok(bytes) => bytes,
            Err(error) => return Err(RecvMessageError::Io(error)),
        };
        let message = match bincode::deserialize::<T>(&bytes) {
            Ok(message) => message,
            Err(_) => return Err(RecvMessageError::MalformedData),
        };

        Ok(message)
    }

    pub fn recv_message_non_blocking<T: DeserializeOwned>(
        &mut self,
    ) -> Result<Option<T>, RecvMessageError> {
        let bytes = match self.recv_bytes_non_blocking() {
            Some(Ok(bytes)) => bytes,
            None => return Ok(None),
            Some(Err(error)) => return Err(RecvMessageError::Io(error)),
        };
        let message = match bincode::deserialize::<T>(&bytes) {
            Ok(message) => message,
            Err(_) => return Err(RecvMessageError::MalformedData),
        };

        Ok(Some(message))
    }
}

impl Client {
    pub fn connect() -> Result<Self, ClientConnectError> {
        let stdout = io::stdout();
        let stdin = io::stdin();

        if stdout.is_terminal() || stdin.is_terminal() {
            return Err(ClientConnectError::ConnectedToTerminal);
        }

        Ok(Self {
            writer: stdout,
            reader: Self::start_reader_thread(stdin),
            _reader_inner: PhantomData,
        })
    }
}
