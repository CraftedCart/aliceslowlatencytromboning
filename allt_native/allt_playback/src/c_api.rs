use crate::player::{Event, Player};
use std::path::PathBuf;
use std::ptr;
use tracing::{error, info};

#[cfg(any(target_os = "linux", target_os = "macos"))]
use std::ffi::c_char;

#[no_mangle]
pub extern "C" fn allt_init_logging() {
    crate::init_logging();
}

#[no_mangle]
pub extern "C" fn allt_player_new() -> *mut Player {
    Box::into_raw(Box::new(Player::new()))
}

/// # Safety
/// `config_dir` must point to null terminated string
#[no_mangle]
#[cfg(any(target_os = "linux", target_os = "macos"))]
pub unsafe extern "C" fn allt_player_load(config_dir: *const c_char) -> *mut Player {
    use std::ffi::{CStr, OsStr};
    use std::os::unix::ffi::OsStrExt;

    let config_dir = CStr::from_ptr(config_dir);
    let config_dir = PathBuf::from(OsStr::from_bytes(config_dir.to_bytes()));
    Box::into_raw(Box::new(Player::load(config_dir)))
}

/// # Safety
/// `config_dir` must point to null terminated string
#[no_mangle]
#[cfg(target_os = "windows")]
pub unsafe extern "C" fn allt_player_load(config_dir: *const libc::wchar_t) -> *mut Player {
    use std::ffi::OsString;
    use std::os::windows::ffi::OsStringExt;

    let len = libc::wcslen(config_dir);
    let config_dir = std::slice::from_raw_parts(config_dir, len);
    let config_dir = PathBuf::from(OsString::from_wide(config_dir));
    Box::into_raw(Box::new(Player::load(config_dir)))
}

/// # Safety
/// `player` must point to a valid `Player`
#[no_mangle]
pub unsafe extern "C" fn allt_player_tick(player: *mut Player) {
    (*player).tick();
}

/// # Safety
/// `player` must point to a valid `Player`, and this must be called on the same thread that the
/// player was created on
#[no_mangle]
pub unsafe extern "C" fn allt_player_start_playback(player: *mut Player) -> bool {
    match (*player).start_playback() {
        Ok(_) => {
            info!("Success with starting playback");
            true
        }
        Err(e) => {
            error!("Failed to start playback: {e:?}");
            false
        }
    }
}

/// # Safety
/// `player` must point to a valid `Player`, and this must be called on the same thread that the
/// player was created on
#[no_mangle]
pub unsafe extern "C" fn allt_player_stop_playback(player: *mut Player) {
    (*player).stop_playback();
}

/// # Safety
/// `player` must point to a valid `Player`, and this must be called on the same thread that the
/// player was created on
#[no_mangle]
pub unsafe extern "C" fn allt_player_drop(player: *mut Player) {
    drop(Box::from_raw(player));
}

/// # Safety
/// `player` must point to a valid `Player`, and this must be called on the same thread that the
/// player was created on
#[no_mangle]
pub unsafe extern "C" fn allt_player_push_play(player: *mut Player) -> bool {
    (*player).push_audio_event(Event::Play).is_ok()
}

/// # Safety
/// `player` must point to a valid `Player`, and this must be called on the same thread that the
/// player was created on
#[no_mangle]
pub unsafe extern "C" fn allt_player_push_stop(player: *mut Player) -> bool {
    (*player).push_audio_event(Event::Stop).is_ok()
}

/// # Safety
/// `player` must point to a valid `Player`, and this must be called on the same thread that the
/// player was created on
#[no_mangle]
pub unsafe extern "C" fn allt_player_push_set_pitch_multiplier(
    player: *mut Player,
    multiplier: f32,
) -> bool {
    (*player)
        .push_audio_event(Event::SetPitchMultiplier(multiplier))
        .is_ok()
}

/// # Safety
/// `player` must point to a valid `Player`, and this must be called on the same thread that the
/// player was created on
#[no_mangle]
pub unsafe extern "C" fn allt_player_push_fade_out(player: *mut Player) -> bool {
    (*player).push_audio_event(Event::FadeOut).is_ok()
}

/// # Safety
/// `player` must point to a valid `Player`, and this must be called on the same thread that the
/// player was created on
#[no_mangle]
pub unsafe extern "C" fn allt_player_push_set_num_trombone_clips(
    player: *mut Player,
    num: usize,
) -> bool {
    (*player)
        .push_audio_event(Event::SetNumTromboneClips(num))
        .is_ok()
}

/// # Safety
/// `player` must point to a valid `Player`, `samples` and `num_samples` must refer to a valid `f32`
/// slice, and this must be called on the same thread that the player was created on
#[no_mangle]
pub unsafe extern "C" fn allt_player_push_set_trombone_clip_samples(
    player: *mut Player,
    index: usize,
    samples: *const f32,
    num_samples: usize,
    channels: usize,
) -> bool {
    let samples = ptr::slice_from_raw_parts(samples, num_samples);
    let mut samples_one_channel = Vec::with_capacity(num_samples / channels);

    // Drop samples that aren't in the 1st channel
    if channels > 1 {
        let mut i = 0;
        while i < num_samples {
            samples_one_channel.push((*samples)[i]);
            i += channels;
        }
    }

    (*player)
        .push_audio_event(Event::SetTromboneClipSamples {
            index,
            samples: samples_one_channel,
        })
        .is_ok()
}

/// # Safety
/// `player` must point to a valid `Player`, and this must be called on the same thread that the
/// player was created on
#[no_mangle]
pub unsafe extern "C" fn allt_player_push_set_clip_to_play(
    player: *mut Player,
    index: usize,
) -> bool {
    (*player)
        .push_audio_event(Event::SetClipToPlay(index))
        .is_ok()
}

/// # Safety
/// `player` must point to a valid `Player`, and this must be called on the same thread that the
/// player was created on
#[no_mangle]
pub unsafe extern "C" fn allt_player_open_config_ui(player: *mut Player) -> bool {
    match (*player).open_config_ui() {
        Ok(()) => true,
        Err(e) => {
            error!("Failed to open config UI: {e:?}");
            false
        }
    }
}
