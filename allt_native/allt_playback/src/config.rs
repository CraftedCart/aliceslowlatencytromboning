use cpal::HostId;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub volume: f32,

    #[serde(serialize_with = "ser_host_id")]
    #[serde(deserialize_with = "de_host_id")]
    pub host_id: Option<HostId>,

    /// Name of the output device to use
    pub output_device: Option<String>,

    pub buffer_size: u32,
    pub trombone_release_seconds: f32,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            volume: 1.0,
            host_id: None,
            output_device: None,
            buffer_size: 128,
            trombone_release_seconds: 1.0 / 18.0,
        }
    }
}

fn ser_host_id<S: Serializer>(value: &Option<HostId>, serializer: S) -> Result<S::Ok, S::Error> {
    match value {
        None => serializer.serialize_none(),
        Some(value) => serializer.serialize_some(value.name()),
    }
}

fn de_host_id<'de, D: Deserializer<'de>>(deserializer: D) -> Result<Option<HostId>, D::Error> {
    let host_name: Option<String> = Deserialize::deserialize(deserializer)?;
    match host_name {
        None => Ok(None),
        Some(host_name) => {
            for host in cpal::ALL_HOSTS {
                if host.name() == host_name {
                    return Ok(Some(*host));
                }
            }

            Err(serde::de::Error::custom("Unknown host_id"))
        }
    }
}
