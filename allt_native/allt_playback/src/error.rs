use crate::player::Event;
use cpal::{
    BackendSpecificError, BuildStreamError, DevicesError, HostUnavailable, PlayStreamError,
    SupportedStreamConfigsError,
};
use rtrb::PushError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum StartPlaybackError {
    #[error("selected audio host unavailable")]
    HostUnavailable,

    #[error("missing output device")]
    OutputDeviceUnavailable,

    #[error("audio backend error: {description}")]
    AudioBackendError { description: String },

    #[error("error getting supported stream configurations")]
    QuerySupportedConfigsError(#[from] SupportedStreamConfigsError),

    #[error("no supported output configurations")]
    NoSupportedConfigsError,

    #[error("failed to build output stream")]
    BuildStreamFailed(#[from] BuildStreamError),

    #[error("failed to start playing from output stream")]
    PlayStreamFailed(#[from] PlayStreamError),
}

#[derive(Error, Debug)]
pub enum PushEventError {
    #[error("event buffer is full")]
    BufferFull,

    #[error("audio playback hasn't been started")]
    NoPlayback,
}

#[derive(Error, Debug)]
pub enum OpenConfigUiError {
    #[error("failed to get module directory")]
    UnknownModuleDir,

    #[error("config UI already open")]
    AlreadyOpen,
}

impl From<HostUnavailable> for StartPlaybackError {
    fn from(_value: HostUnavailable) -> Self {
        Self::HostUnavailable
    }
}

impl From<DevicesError> for StartPlaybackError {
    fn from(value: DevicesError) -> Self {
        match value {
            DevicesError::BackendSpecific {
                err: BackendSpecificError { description },
            } => StartPlaybackError::AudioBackendError { description },
        }
    }
}

impl From<PushError<Event>> for PushEventError {
    fn from(value: PushError<Event>) -> Self {
        match value {
            PushError::Full(_) => Self::BufferFull,
        }
    }
}
