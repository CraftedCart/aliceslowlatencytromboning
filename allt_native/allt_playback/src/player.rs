use crate::config::Config;
use crate::error::{OpenConfigUiError, PushEventError, StartPlaybackError};
use crate::ipc;
use crate::ipc::{IpcRequest, IpcResponseSendConfig};
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use cpal::{BufferSize, SampleRate, Stream, StreamConfig, SupportedBufferSize};
use dasp_interpolate::linear::Linear;
use dasp_signal::interpolate::Converter;
use dasp_signal::Signal;
use rtrb::{Producer, RingBuffer};
use std::fs::File;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use tracing::{debug, error, info};

pub struct Player {
    playback: Option<AudioPlayback>,
    config_dir: Option<PathBuf>,
    config: Config,
    config_ui_server: Option<ipc::Server>,
}

struct AudioPlayback {
    _stream: Stream,
    tx: Producer<Event>,
}

pub enum Event {
    Play,
    Stop,
    SetPitchMultiplier(f32),

    /// Fade out the currently playing sound (for when the user releases the key)
    FadeOut,

    SetNumTromboneClips(usize),
    SetTromboneClipSamples {
        index: usize,
        samples: Vec<f32>,
    },
    SetClipToPlay(usize),
}

#[derive(Clone, Default)]
pub struct TromboneClip {
    samples: Vec<f32>,
}

#[derive(Default)]
struct TromboneSignal {
    pub trombone_clips: Vec<TromboneClip>,
    pub current_playing_clip_index: usize,
    pub current_playing_clip_sample_index: usize,
}

impl Signal for TromboneSignal {
    type Frame = f32;

    fn next(&mut self) -> Self::Frame {
        if self.trombone_clips.len() > self.current_playing_clip_index
            && self.trombone_clips[self.current_playing_clip_index]
                .samples
                .len()
                > self.current_playing_clip_sample_index
        {
            let trombone_sample = &self.trombone_clips[self.current_playing_clip_index].samples;
            let sample = trombone_sample[self.current_playing_clip_sample_index];
            self.current_playing_clip_sample_index += 1;
            if self.current_playing_clip_sample_index >= trombone_sample.len() {
                self.current_playing_clip_sample_index = 0;
            }

            sample
        } else {
            0.0
        }
    }
}

impl Player {
    pub fn new() -> Self {
        Self {
            playback: None,
            config_dir: None,
            config: Config::default(),
            config_ui_server: None,
        }
    }

    pub fn load(config_dir: PathBuf) -> Self {
        let config = Self::read_config_or_default(&config_dir);
        Self {
            playback: None,
            config_dir: Some(config_dir),
            config,
            config_ui_server: None,
        }
    }

    fn read_config_or_default(config_dir: &Path) -> Config {
        let config_path = config_dir.join("alices_low_latency_tromboning.json");
        info!("Reading config from {config_path:?}");
        let reader = match File::open(config_path) {
            Ok(reader) => reader,
            Err(e) => {
                error!("Error reading config: {e:?}");
                return Config::default();
            }
        };

        let config = serde_json::from_reader::<_, Config>(reader).unwrap_or_else(|e| {
            error!("Error deserializing config: {e:?}");
            Config::default()
        });

        info!("Loaded config: {config:#?}");
        config
    }

    fn save_config(&self) {
        let config_path = self
            .config_dir
            .as_ref()
            .expect("can't save with no config dir")
            .join("alices_low_latency_tromboning.json");
        let file = match File::create(config_path) {
            Ok(file) => file,
            Err(e) => {
                error!("Error opening config file for writing: {e:?}");
                return;
            }
        };
        match serde_json::to_writer(file, &self.config) {
            Ok(()) => {}
            Err(e) => {
                error!("Error writing config file: {e:?}");
            }
        }
    }

    /// If playback has already started, it stops that one when starting the new one
    pub fn start_playback(&mut self) -> Result<(), StartPlaybackError> {
        info!("Attempting to start playback");

        let desired_sample_rate = SampleRate(44100);
        let desired_buffer_size = self.config.buffer_size;
        let desired_channels = 1;

        debug!("Finding audio host");
        let host = match self.config.host_id {
            None => cpal::default_host(),
            Some(host_id) => cpal::host_from_id(host_id)?,
        };

        debug!("Finding output device");
        let device = match &self.config.output_device {
            None => host
                .default_output_device()
                .ok_or(StartPlaybackError::OutputDeviceUnavailable),
            Some(device_name) => host
                .output_devices()?
                .find(|device| {
                    device
                        .name()
                        .map(|name| name == *device_name)
                        .unwrap_or(false)
                })
                .ok_or(StartPlaybackError::OutputDeviceUnavailable),
        }?;

        // Log supported stream configs, just for debugging
        // And see if we can find a config that matches what we desire
        let mut stream_config = None;
        debug!("===== Supported stream configs =====");
        for config in device.supported_output_configs()? {
            debug!("{config:?}");

            if ((stream_config.is_some() && config.channels() == desired_channels)
                || stream_config.is_none())
                && config.min_sample_rate() <= desired_sample_rate
                && config.max_sample_rate() >= desired_sample_rate
            {
                let matching_buffer_size = match config.buffer_size() {
                    SupportedBufferSize::Range { min, max } => {
                        *min <= desired_buffer_size && *max >= desired_buffer_size
                    }
                    SupportedBufferSize::Unknown => true,
                };
                if matching_buffer_size {
                    // Good config!
                    stream_config = Some(StreamConfig {
                        channels: config.channels(),
                        sample_rate: desired_sample_rate,
                        buffer_size: BufferSize::Fixed(desired_buffer_size),
                    });
                }
            }
        }
        debug!("====================================");

        let stream_config = match stream_config {
            None => {
                info!("Desired stream config not found - using first working supported one");
                // TODO: Should we use default_output_config instead?
                let supported_range = device
                    .supported_output_configs()?
                    .next()
                    .ok_or(StartPlaybackError::NoSupportedConfigsError)?;

                StreamConfig {
                    channels: supported_range.channels(),
                    sample_rate: if supported_range.min_sample_rate() <= SampleRate(44100)
                        && supported_range.max_sample_rate() >= SampleRate(44100)
                    {
                        SampleRate(44100)
                    } else if supported_range.min_sample_rate() <= SampleRate(48000)
                        && supported_range.max_sample_rate() >= SampleRate(48000)
                    {
                        SampleRate(48000)
                    } else {
                        supported_range.min_sample_rate()
                    },
                    buffer_size: BufferSize::Fixed(match supported_range.buffer_size() {
                        SupportedBufferSize::Range { min, max } => {
                            if *min <= desired_buffer_size && *max >= desired_buffer_size {
                                desired_buffer_size
                            } else if *min <= 256 && *max >= 256 {
                                256
                            } else {
                                *min
                            }
                        }
                        SupportedBufferSize::Unknown => desired_buffer_size,
                    }),
                }
            }
            Some(stream_config) => stream_config,
        };

        info!("Using stream config: {stream_config:#?}");

        let (tx, mut rx) = RingBuffer::new(64);

        let mut playing = false;

        // Volume of the trombone, fades from 1 -> 0 when releasing a key
        let mut trombone_volume = 1.0_f32;

        // Volume the user set in the config UI
        let user_volume = self.config.volume;

        // Are we currently fading out the trombone sound because the user released a key?
        let mut is_fading_out = false;

        // Trombone audio generator
        let trombone_signal = TromboneSignal::default();

        // For pitch shifting
        let linear = Linear::new(0.0, 0.0);
        let trombone_signal = Converter::scale_playback_hz(trombone_signal, linear, 1.0);

        // Resample to our desired sample rate
        let linear = Linear::new(0.0, 0.0);
        let mut trombone_signal = Converter::from_hz_to_hz(
            trombone_signal,
            linear,
            44100.0,
            stream_config.sample_rate.0 as f64,
        );

        let trombone_release_seconds = self.config.trombone_release_seconds;

        debug!("Building output stream");
        let stream = device.build_output_stream(
            &stream_config,
            move |data: &mut [f32], _: &cpal::OutputCallbackInfo| {
                while let Ok(ev) = rx.pop() {
                    match ev {
                        Event::Play => {
                            playing = true;
                            is_fading_out = false;
                            trombone_volume = 1.0;
                            trombone_signal
                                .source_mut()
                                .source_mut()
                                .current_playing_clip_sample_index = 0;
                        }
                        Event::Stop => {
                            playing = false;
                        }
                        Event::SetPitchMultiplier(multiplier) => {
                            trombone_signal
                                .source_mut()
                                .set_playback_hz_scale(multiplier as f64);
                        }
                        Event::FadeOut => {
                            is_fading_out = true;
                        }
                        Event::SetNumTromboneClips(num) => {
                            trombone_signal
                                .source_mut()
                                .source_mut()
                                .trombone_clips
                                .resize(num, TromboneClip::default());
                        }
                        Event::SetTromboneClipSamples { index, samples } => {
                            trombone_signal.source_mut().source_mut().trombone_clips[index]
                                .samples = samples;
                        }
                        Event::SetClipToPlay(index) => {
                            trombone_signal
                                .source_mut()
                                .source_mut()
                                .current_playing_clip_index = index;
                        }
                    }
                }

                for frame in data.chunks_mut(stream_config.channels as usize) {
                    if is_fading_out {
                        // Fade out over a short while (1/18 seconds by default)
                        trombone_volume -=
                            (1.0 / trombone_release_seconds) / stream_config.sample_rate.0 as f32;
                        if trombone_volume < 0.0 {
                            trombone_volume = 0.0;
                        }
                    }

                    if playing {
                        let next_sample = trombone_signal.next() * trombone_volume * user_volume;
                        for sample in frame {
                            *sample = next_sample;
                        }
                    } else {
                        for sample in frame {
                            *sample = 0.0;
                        }
                    }
                }
            },
            move |err| {
                error!("{err:?}");
            },
            None,
        )?;

        debug!("Playing output stream");
        stream.play()?;

        self.playback = Some(AudioPlayback {
            _stream: stream,
            tx,
        });

        Ok(())
    }

    pub fn stop_playback(&mut self) {
        info!("Stopping playback");
        self.playback = None;
    }

    pub fn push_audio_event(&mut self, event: Event) -> Result<(), PushEventError> {
        match &mut self.playback {
            None => Err(PushEventError::NoPlayback),
            Some(playback) => playback.tx.push(event).map_err(Into::into),
        }
    }

    /// Call this regularly - needed for the config UI to function and not hang
    pub fn tick(&mut self) {
        if let Some(server) = &mut self.config_ui_server {
            match server.recv_message_non_blocking::<IpcRequest>() {
                Ok(Some(message)) => match message {
                    IpcRequest::RequestConfig => {
                        if let Err(e) =
                            server.send_message(&IpcResponseSendConfig(self.config.clone()))
                        {
                            error!("Error sending message to config UI: {e:?}");
                            self.config_ui_server = None;
                        }
                    }
                    IpcRequest::SaveConfig(config) => {
                        self.config = config;
                        self.save_config();
                    }
                },
                Ok(None) => {
                    // Nothing to do
                }
                Err(e) => {
                    error!("Error receiving message from config UI: {e:?}");
                    self.config_ui_server = None;
                }
            }
        }
    }

    pub fn open_config_ui(&mut self) -> Result<(), OpenConfigUiError> {
        if self.is_config_ui_open() {
            return Err(OpenConfigUiError::AlreadyOpen);
        }

        #[cfg(any(target_os = "linux", target_os = "macos"))]
        const EXECUTABLE_NAME: &str = "allt_config_ui";
        #[cfg(target_os = "windows")]
        const EXECUTABLE_NAME: &str = "allt_config_ui.exe";

        let dylib_path = crate::current_dylib_path().ok_or(OpenConfigUiError::UnknownModuleDir)?;
        let dylib_dir = dylib_path
            .parent()
            .ok_or(OpenConfigUiError::UnknownModuleDir)?;
        let config_executable = dylib_dir.join(EXECUTABLE_NAME);

        let mut child = Command::new(config_executable)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::inherit())
            .spawn()
            .expect("failed to execute process");

        let stdout = child.stdout.take().unwrap();
        let stdin = child.stdin.take().unwrap();
        let server = ipc::Server::from_streams(stdin, stdout);

        self.config_ui_server = Some(server);

        Ok(())
    }

    pub fn is_config_ui_open(&self) -> bool {
        self.config_ui_server.is_some()
    }
}
