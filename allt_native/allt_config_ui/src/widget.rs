use crate::Message;
use iced::advanced::layout::{Limits, Node};
use iced::advanced::renderer::Style;
use iced::advanced::widget::Tree;
use iced::advanced::{Clipboard, Layout, Shell, Widget};
use iced::event::Status;
use iced::mouse::{Cursor, Interaction};
use iced::{Element, Event, Length, Rectangle, Size, Vector};

pub struct InfoHover<'a, Theme = iced::Theme, Renderer = iced::Renderer>
where
    Renderer: iced::advanced::text::Renderer,
{
    content: Element<'a, Message, Theme, Renderer>,
    message: &'static str,
}

impl<'a, Theme, Renderer> Widget<Message, Theme, Renderer> for InfoHover<'a, Theme, Renderer>
where
    Renderer: iced::advanced::text::Renderer,
{
    fn size(&self) -> Size<Length> {
        self.content.as_widget().size()
    }

    fn layout(&self, tree: &mut Tree, renderer: &Renderer, limits: &Limits) -> Node {
        self.content
            .as_widget()
            .layout(&mut tree.children[0], renderer, limits)
    }

    fn draw(
        &self,
        tree: &Tree,
        renderer: &mut Renderer,
        theme: &Theme,
        style: &Style,
        layout: Layout<'_>,
        cursor: Cursor,
        viewport: &Rectangle,
    ) {
        self.content.as_widget().draw(
            &tree.children[0],
            renderer,
            theme,
            style,
            layout,
            cursor,
            viewport,
        )
    }

    fn children(&self) -> Vec<Tree> {
        vec![Tree::new(&self.content)]
    }

    fn diff(&self, tree: &mut Tree) {
        tree.diff_children(&[self.content.as_widget()]);
    }

    fn on_event(
        &mut self,
        state: &mut Tree,
        event: Event,
        layout: Layout<'_>,
        cursor: Cursor,
        renderer: &Renderer,
        clipboard: &mut dyn Clipboard,
        shell: &mut Shell<'_, Message>,
        viewport: &Rectangle,
    ) -> Status {
        if cursor.is_over(layout.bounds()) {
            shell.publish(Message::SetInfoHoverText(self.message));
        }

        self.content.as_widget_mut().on_event(
            &mut state.children[0],
            event,
            layout,
            cursor,
            renderer,
            clipboard,
            shell,
            viewport,
        )
    }

    fn mouse_interaction(
        &self,
        state: &Tree,
        layout: Layout<'_>,
        cursor: Cursor,
        viewport: &Rectangle,
        renderer: &Renderer,
    ) -> Interaction {
        self.content.as_widget().mouse_interaction(
            &state.children[0],
            layout,
            cursor,
            viewport,
            renderer,
        )
    }

    fn overlay<'b>(
        &'b mut self,
        state: &'b mut Tree,
        layout: Layout<'_>,
        renderer: &Renderer,
        translation: Vector,
    ) -> Option<iced::advanced::overlay::Element<'b, Message, Theme, Renderer>> {
        self.content
            .as_widget_mut()
            .overlay(&mut state.children[0], layout, renderer, translation)
    }
}

impl<'a, Theme: 'a, Renderer> From<InfoHover<'a, Theme, Renderer>>
    for Element<'a, Message, Theme, Renderer>
where
    Message: 'a,
    Renderer: iced::advanced::text::Renderer + 'a,
{
    fn from(info_hover: InfoHover<'a, Theme, Renderer>) -> Element<'a, Message, Theme, Renderer> {
        Element::new(info_hover)
    }
}

pub fn info_hover<'a, Theme, Renderer>(
    content: impl Into<Element<'a, Message, Theme, Renderer>>,
    message: &'static str,
) -> InfoHover<'a, Theme, Renderer>
where
    Renderer: iced::advanced::text::Renderer,
{
    InfoHover {
        content: content.into(),
        message,
    }
}
