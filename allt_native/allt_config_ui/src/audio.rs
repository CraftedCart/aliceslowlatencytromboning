use allt_playback::cpal;
use allt_playback::cpal::traits::{DeviceTrait, HostTrait};
use allt_playback::cpal::HostId;
use std::fmt::{Display, Formatter};
use tracing::error;

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct AudioHost(pub HostId);

impl Display for AudioHost {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.0.name())
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct AudioDevice {
    pub name: String,
}

impl Display for AudioDevice {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.name)
    }
}

pub fn get_hosts() -> Vec<AudioHost> {
    cpal::available_hosts().into_iter().map(AudioHost).collect()
}

pub fn get_devices(host_id: HostId) -> Vec<AudioDevice> {
    let host = match cpal::host_from_id(host_id) {
        Ok(host) => host,
        Err(e) => {
            error!("Error getting host when querying output devices: {e:#?}");
            return Vec::new();
        }
    };

    let devices = match host.output_devices() {
        Ok(devices) => devices,
        Err(e) => {
            error!("Error getting output devices when querying output devices: {e:#?}");
            return Vec::new();
        }
    };

    devices
        .filter_map(|device| match device.name() {
            Ok(name) => Some(AudioDevice { name }),
            Err(e) => {
                error!("Error getting device name when querying output devices: {e:#?}");
                None
            }
        })
        .collect()
}
