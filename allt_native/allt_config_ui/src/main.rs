#![windows_subsystem = "windows"]

mod audio;
mod widget;

use crate::audio::{AudioDevice, AudioHost};
use allt_playback::config::Config;
use allt_playback::ipc::{IpcRequest, IpcResponseSendConfig};
use allt_playback::{cpal, ipc};
use iced::widget::{
    button, column, container, horizontal_space, pick_list, row, slider, text, text_input,
    vertical_space,
};
use iced::window;
use iced::{Alignment, Command, Element, Length, Settings};
use tracing::info;
use widget::info_hover;

struct ConfigUi {
    state: UiMainState,
    client: Option<ipc::Client>,
}

struct UiEditConfigState {
    volume_text: String,
    volume_value: f32,

    buffer_size_text: String,
    buffer_size_value: u32,

    release_millis_text: String,
    release_millis_value: f32,

    info_hover_text: &'static str,

    available_hosts: Vec<AudioHost>,
    host: Option<AudioHost>,

    available_devices: Vec<AudioDevice>,
    device: Option<AudioDevice>,
}

enum UiMainState {
    IpcError(String),
    EditConfig(UiEditConfigState),
}

#[derive(Debug, Clone)]
enum Message {
    Exit,
    SaveAndExit,
    SetInfoHoverText(&'static str),
    VolumeTextChanged(String),
    VolumeValueChanged(f32),
    BufferSizeTextChanged(String),
    BufferSizeValueChanged(u32),
    ReleaseMillisTextChanged(String),
    ReleaseMillisValueChanged(f32),
    HostChanged(AudioHost),
    DeviceChanged(AudioDevice),
}

impl Default for ConfigUi {
    fn default() -> Self {
        fn create_client_and_state() -> (Option<ipc::Client>, UiMainState) {
            let client = ipc::Client::connect();
            match client {
                Ok(mut client) => {
                    if let Err(e) = client.send_message(&IpcRequest::RequestConfig) {
                        return (None, UiMainState::IpcError(e.to_string()));
                    }
                    let config = match client.recv_message::<IpcResponseSendConfig>() {
                        Ok(message) => message.0,
                        Err(e) => return (None, UiMainState::IpcError(e.to_string())),
                    };
                    let available_devices =
                        audio::get_devices(config.host_id.unwrap_or(cpal::default_host().id()));

                    let state = UiEditConfigState {
                        volume_text: format!("{:.2}", config.volume),
                        volume_value: config.volume,
                        buffer_size_text: config.buffer_size.to_string(),
                        buffer_size_value: config.buffer_size,
                        release_millis_text: format!(
                            "{:.1}",
                            config.trombone_release_seconds * 1000.0
                        ),
                        release_millis_value: config.trombone_release_seconds * 1000.0,

                        info_hover_text: "",
                        available_hosts: audio::get_hosts(),
                        host: config.host_id.map(AudioHost),
                        available_devices,
                        device: config.output_device.map(|name| AudioDevice { name }),
                    };

                    (Some(client), UiMainState::EditConfig(state))
                }
                Err(error) => (None, UiMainState::IpcError(error.to_string())),
            }
        }

        let (client, state) = create_client_and_state();

        ConfigUi { state, client }
    }
}

impl ConfigUi {
    fn update(&mut self, message: Message) -> Command<Message> {
        match message {
            Message::Exit => window::close(window::Id::MAIN),
            message => match &mut self.state {
                UiMainState::IpcError(_) => panic!("unexpected UI state"),
                UiMainState::EditConfig(state) => match message {
                    Message::SaveAndExit => {
                        let config = Config {
                            volume: state.volume_value,
                            host_id: state.host.map(|host| host.0),
                            output_device: state.device.as_ref().map(|device| device.name.clone()),
                            buffer_size: state.buffer_size_value,
                            trombone_release_seconds: state.release_millis_value / 1000.0,
                        };
                        if let Err(e) = self
                            .client
                            .as_mut()
                            .unwrap()
                            .send_message(&IpcRequest::SaveConfig(config))
                        {
                            self.state = UiMainState::IpcError(e.to_string());
                            Command::none()
                        } else {
                            window::close(window::Id::MAIN)
                        }
                    }
                    Message::SetInfoHoverText(text) => {
                        state.info_hover_text = text;
                        Command::none()
                    }
                    Message::VolumeTextChanged(mut text) => {
                        if let Ok(value) = text.parse() {
                            state.volume_value = value;

                            if state.volume_value < 0.0 {
                                state.volume_value = 0.0;
                                text = "0.00".to_string();
                            } else if state.volume_value > 1.0 {
                                state.volume_value = 1.0;
                                text = "1.00".to_string();
                            }
                        }
                        state.volume_text = text;

                        Command::none()
                    }
                    Message::VolumeValueChanged(value) => {
                        state.volume_value = value;
                        state.volume_text = format!("{value:.2}");
                        Command::none()
                    }
                    Message::BufferSizeTextChanged(text) => {
                        if let Ok(value) = text.parse() {
                            state.buffer_size_value = value;
                        }
                        state.buffer_size_text = text;

                        Command::none()
                    }
                    Message::BufferSizeValueChanged(value) => {
                        state.buffer_size_value = value;
                        state.buffer_size_text = value.to_string();
                        Command::none()
                    }
                    Message::ReleaseMillisTextChanged(mut text) => {
                        if let Ok(value) = text.parse() {
                            state.release_millis_value = value;

                            if state.release_millis_value < 0.0 {
                                state.release_millis_value = 0.0;
                                text = "0.0".to_string();
                            }
                        }
                        state.release_millis_text = text;

                        Command::none()
                    }
                    Message::ReleaseMillisValueChanged(value) => {
                        state.release_millis_value = value;
                        state.release_millis_text = format!("{value:.1}");
                        Command::none()
                    }
                    Message::HostChanged(host) => {
                        state.host = Some(host);
                        state.available_devices = audio::get_devices(host.0);
                        state.device = None;
                        Command::none()
                    }
                    Message::DeviceChanged(device) => {
                        state.device = Some(device);
                        Command::none()
                    }
                    _ => panic!("unexpected UI state"),
                },
            },
        }
    }

    fn view(&self) -> Element<Message> {
        match &self.state {
            UiMainState::IpcError(message) => self.view_ipc_error(message),
            UiMainState::EditConfig(state) => state.view_config_editor(),
        }
    }
}

impl ConfigUi {
    #[rustfmt::skip]
    fn view_ipc_error<'a>(&'a self, message: &'a str) -> Element<'a, Message> {
        let content = column![
            "Error communicating with game",
            message,
            button("Exit").on_press(Message::Exit),
        ]
        .align_items(Alignment::Center)
        .spacing(10);

        container(content)
            .width(Length::Fill)
            .height(Length::Fill)
            .center_x()
            .center_y()
            .padding(20)
            .into()
    }
}

impl UiEditConfigState {
    #[rustfmt::skip]
    fn view_config_editor(&self) -> Element<Message> {
        let content = column![
            info_hover(
                row![
                    column!["Trombone volume", text("Default: 1.0").size(12)],
                    slider(0.0..=1.0, self.volume_value, Message::VolumeValueChanged).step(0.01_f32).shift_step(0.001_f32),
                    text_input("", &self.volume_text).width(64).on_input(Message::VolumeTextChanged),
                ]
                .spacing(10)
                .align_items(Alignment::Center),
                "The volume of the trombone",
            ),

            info_hover(
                row![
                    "Audio host",
                    pick_list(&*self.available_hosts, self.host, Message::HostChanged),
                ]
                .spacing(10)
                .align_items(Alignment::Center),
                "The audio API to use\n\
                On Windows, ASIO generally provides lower latency than WASAPI, but requires your audio setup to be capable of working with ASIO",
            ),

            info_hover(
                row![
                    "Audio device",
                    pick_list(&*self.available_devices, self.device.clone(), Message::DeviceChanged),
                ]
                .spacing(10)
                .align_items(Alignment::Center),
                "The audio device to output to\n",
            ),

            info_hover(
                row![
                    column!["Buffer size", text("Default: 128").size(12)],
                    slider(32..=1024, self.buffer_size_value, Message::BufferSizeValueChanged).step(32_u32).shift_step(1_u32),
                    text_input("", &self.buffer_size_text).width(64).on_input(Message::BufferSizeTextChanged),
                    "samples",
                ]
                .spacing(10)
                .align_items(Alignment::Center),
                "Lower values will mean less latency, however may lead to some crackling if set too low and your computer can't keep up\n\
                This is the target buffer size to hit, it's not guaranteed that the buffer size will be exactly what is requested",
            ),

            info_hover(
                row![
                    column!["Trombone release time", text("Default: 55.6").size(12)],
                    slider(0.0..=100.0, self.release_millis_value, Message::ReleaseMillisValueChanged).step(1_f32).shift_step(0.1_f32),
                    text_input("", &self.release_millis_text).width(64).on_input(Message::ReleaseMillisTextChanged),
                    "ms",
                ]
                .spacing(10)
                .align_items(Alignment::Center),
                "Hold long it takes for the trombone noise to fade away after releasing any key",
            ),

            vertical_space(),

            self.info_hover_text,

            row![
                horizontal_space(),
                button("Exit without saving").on_press(Message::Exit),
                button("Save and exit").on_press(Message::SaveAndExit),
            ]
            .spacing(10),
        ]
            .spacing(10);

        container(content)
            .width(Length::Fill)
            .height(Length::Fill)
            .padding(20)
            .into()
    }
}

pub fn main() -> iced::Result {
    allt_playback::init_logging();
    info!("Opening config UI");

    let mut settings = Settings::default();
    settings.window.size.width = 640.0;
    settings.window.size.height = 480.0;

    iced::program(
        "Alice's Low Latency Tromboning - Configure",
        ConfigUi::update,
        ConfigUi::view,
    )
    .settings(settings)
    .run()
}
