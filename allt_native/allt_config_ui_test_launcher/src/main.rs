use allt_playback::player::Player;

fn main() {
    let mut player = Player::new();
    player.open_config_ui().expect("failed to open config UI");

    while player.is_config_ui_open() {
        player.tick();
        std::thread::yield_now();
    }
}
