v0.1.5 - 2024-07-03
===================

- Fix bug where ALLT would try to output mono audio when your sound device didn't support it, causing no trombone audio

v0.1.4 - 2024-05-06
===================

- Tentative fix for a bug I couldn't reproduce where it would try to load trombone samples more than once, causing a lot
  of stuttering
- Also less log spam when starting a chart

v0.1.3 - 2024-03-31
===================

- Fix some crashes when opening the config UI
    - There was a bug in the audio library I use, CPAL, that caused it to panic when querying output devices on some
      systems. Updating CPAL did the trick.

v0.1.2 - 2024-03-27
===================

- Use software rendering when drawing the config UI - this should alleviate issues some people were having with Vulkan
  rendering causing the config UI to crash
- The config UI also now follows the system theme for whether it should be light or dark

v0.1.1 - 2024-03-16
===================

- Be more lenient in what sample rates/channel numbers we can play with, since it turns out the initial release worked
  in Wine but not on Windows
- Fix crash if playing trombone sounds through ALLT failed

v0.1.0 - 2024-03-15
===================

Initial release
