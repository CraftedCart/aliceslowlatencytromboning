Alice's Low Latency Tromboning
==============================

A Trombone Champ mod for lower latency trombone sounds

Currently only supports Windows and WASAPI (no ASIO yet)

## Configuring

Open the settings menu in-game, and there should be an "ALLT Settings" button. This will pop-out a new window where you
can configure stuff.
