#!/bin/sh

# Bail on error
set -e

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)

echo ":::::::: Building C# mod ::::::::"
dotnet restore "$SCRIPT_DIR/AlicesLowLatencyTromboning/AlicesLowLatencyTromboning.sln"
dotnet build "$SCRIPT_DIR/AlicesLowLatencyTromboning/AlicesLowLatencyTromboning.sln" /p:Configuration=Release

echo ":::::::: Building allt_playback ::::::::"
cross build --manifest-path "$SCRIPT_DIR/allt_native/Cargo.toml" -p allt_playback --target x86_64-pc-windows-gnu

echo ":::::::: Building allt_config_ui ::::::::"
cross build --manifest-path "$SCRIPT_DIR/allt_native/Cargo.toml" -p allt_config_ui --target x86_64-pc-windows-gnu

echo ":::::::: Bundling Thunderstore package ::::::::"
mkdir -p "$SCRIPT_DIR/dist/BepInEx/plugins"
cp -v "$SCRIPT_DIR/thunderstore/"* "$SCRIPT_DIR/dist"
cp -v "$SCRIPT_DIR/AlicesLowLatencyTromboning/AlicesLowLatencyTromboning/bin/Release/net472/AlicesLowLatencyTromboning.dll" "$SCRIPT_DIR/dist/BepInEx/plugins"
cp -v "$SCRIPT_DIR/allt_native/target/x86_64-pc-windows-gnu/debug/"{allt_playback.dll,allt_config_ui.exe} "$SCRIPT_DIR/dist/BepInEx/plugins"

echo ":::::::: Done! ::::::::"
echo "Zip up the contents of 'dist' to create an r2modman/thunderstore package"
