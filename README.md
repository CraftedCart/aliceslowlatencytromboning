Alice's Low Latency Tromboning
==============================

A Trombone Champ mod for lower latency trombone sounds

## Building

- Build the C# project `AlicesLowLatencyTromboning` - which will spit out `AlicesLowLatencyTromboning.dll`
- Build the Rust projects `allt_playback` and `allt_config_ui`
- Plonk the C# DLL, the `allt_playback` DLL, and the `allt_config_ui` executable inside the same folder within your
  BepInEx plugins folder

## Configuring

Open the settings menu in-game, and there should be an "ALLT Settings" button. This will pop-out a new window where you
can configure stuff.
