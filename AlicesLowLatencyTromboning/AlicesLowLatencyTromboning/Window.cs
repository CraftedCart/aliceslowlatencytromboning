// ReSharper disable InconsistentNaming

using System;
using System.Runtime.InteropServices;

namespace AlicesLowLatencyTromboning
{
    internal static class Window
    {
        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        private static extern IntPtr GetActiveWindow();

        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_RESTORE = 9;

        internal static void Minimize()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                ShowWindow(GetActiveWindow(), SW_SHOWMINIMIZED);
            }
            else
            {
                AlltPlugin.Instance.Logger.LogWarning("Window.Minimize() not supported on this platform");
            }
        }

        internal static void Restore()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                ShowWindow(GetActiveWindow(), SW_RESTORE);
            }
            else
            {
                AlltPlugin.Instance.Logger.LogWarning("Window.Restore() not supported on this platform");
            }
        }
    }
}
