﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using BaboonAPI.Hooks.Initializer;
using BepInEx;
using BepInEx.Logging;
using HarmonyLib;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace AlicesLowLatencyTromboning
{
    [BepInPlugin("craftedcart.low_latency_tromboning", "Alice's Low Latency Tromboning", "0.1.5.0")]
    [BepInDependency("ch.offbeatwit.baboonapi.plugin")]
    public class AlltPlugin : BaseUnityPlugin
    {
        [DllImport("allt_playback.dll")]
        private static extern void allt_init_logging();

        private static AlltPlugin? _instance;
        internal static AlltPlugin Instance
        {
            get
            {
                if (_instance is null)
                {
                    throw new NullReferenceException("Instance not initialized - accessing too early?");
                }

                return _instance;
            }
        }

        internal new ManualLogSource Logger => base.Logger;

        private AlltPlayer? _player;
        internal AlltPlayer Player
        {
            get
            {
                if (_player is null)
                {
                    throw new NullReferenceException("Player not initialized - accessing too early?");
                }

                return _player;
            }
        }

        private readonly Harmony _harmony = new("craftedcart.low_latency_tromboning");

        private void Awake()
        {
            _instance = this;
            GameInitializationEvent.Register(Info, TryInitialize);
        }

        private void TryInitialize()
        {
            allt_init_logging();

            _harmony.PatchAll(typeof(GameControllerPatch));
            SceneManager.activeSceneChanged += GameControllerPatch.OnSceneChanged;
            SceneManager.activeSceneChanged += OnSceneChanged;

            _player = AlltPlayer.Load();
        }

        private void Update()
        {
            _player?.Tick();
        }

        private void OnSceneChanged(Scene oldScene, Scene newScene)
        {
            if (newScene.name == "home")
            {
                var mainCanvasGo = newScene.GetRootGameObjects().First(go => go.name == "MainCanvas");
                if (mainCanvasGo == null)
                {
                    Logger.LogError("Failed to find main canvas");
                    return;
                }

                // Setup settings button
                var timingPanelGo = mainCanvasGo.transform.Find("SettingsPanel/Settings/TIMING");
                if (timingPanelGo == null)
                {
                    Logger.LogError("Failed to find timing panel to add settings button to");
                    return;
                }

                var settingsButtonGo = new GameObject("AlltSettingsButton", typeof(RectTransform), typeof(Button), typeof(Image));
                var settingsButtonTf = (RectTransform)settingsButtonGo.transform;
                var settingsButtonButton = settingsButtonGo.GetComponent<Button>();
                var settingsButtonImage = settingsButtonGo.GetComponent<Image>();
                settingsButtonTf.SetParent(timingPanelGo.transform, false);
                settingsButtonTf.anchorMin = new Vector2(1f, 0f);
                settingsButtonTf.anchorMax = new Vector2(1f, 0f);
                settingsButtonTf.offsetMin = new Vector2(-208f, 8f);
                settingsButtonTf.offsetMax = new Vector2(-25f, 42f);
                settingsButtonButton.targetGraphic = settingsButtonImage;
                settingsButtonImage.color = new Color(0.914f, 0.212f, 0.337f, 1.000f);
                settingsButtonButton.onClick.AddListener(LaunchAlltConfigUi);

                var settingsTextGo = new GameObject("AlltSettingsButtonText", typeof(RectTransform), typeof(TextMeshProUGUI));
                var settingsTextTf = (RectTransform)settingsTextGo.transform;
                settingsTextTf.SetParent(settingsButtonTf, false);
                settingsTextTf.anchorMin = new Vector2(0.5f, 0.5f);
                settingsTextTf.anchorMax = new Vector2(0.5f, 0.5f);
                var settingsTextText = settingsTextTf.GetComponent<TextMeshProUGUI>();
                settingsTextText.text = "ALLT Settings";
                settingsTextText.color = Color.white;
                settingsTextText.enableWordWrapping = false;
                settingsTextText.alignment = TextAlignmentOptions.Center;
                settingsTextText.fontSize = 24f;
            }
        }

        private void LaunchAlltConfigUi()
        {
            Window.Minimize();
            Player.OpenConfigUi();
        }
    }
}
