// ReSharper disable InconsistentNaming

using HarmonyLib;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AlicesLowLatencyTromboning
{
    public class GameControllerPatch
    {
        [HarmonyPatch(typeof(GameController), nameof(GameController.Start))]
        [HarmonyPostfix]
        static void Start_Post(GameController __instance)
        {
            var player = AlltPlugin.Instance.Player;
            player.StartPlayback();

            AlltPlugin.Instance.Logger.LogInfo("Sending trombone clip samples to ALLT Playback");

            // This looks kinda icky, but it's the same way the game does it in `loadSoundBundleResources`
            var tromboneClips = __instance.soundSets.transform.GetChild(0).gameObject.GetComponent<AudioClipsTromb>();

            player.PushSetNumTromboneClips((nuint)tromboneClips.tclips.Length);
            for (var i = 0; i < tromboneClips.tclips.Length; i++)
            {
                var clip = tromboneClips.tclips[i];
                var data = new float[clip.samples * clip.channels];
                if (clip.GetData(data, 0))
                {
                    player.PushSetTromboneClipSamples((nuint)i, data, (nuint)clip.channels);
                }
                else
                {
                    AlltPlugin.Instance.Logger.LogError($"Failed to get trombone clip {i} samples");
                }
            }
        }

        [HarmonyPatch(typeof(GameController), nameof(GameController.playNote))]
        [HarmonyPostfix]
        static void PlayNote_Post(GameController __instance)
        {
            // Logic copied from Trombone Champ
            var num1 = 9999f;
            var index1 = 0;
            for (var index2 = 0; index2 < 15; ++index2)
            {
                var num2 = Mathf.Abs(__instance.notelinepos[index2] - __instance.pointer.transform.localPosition.y);
                if (!(num2 < num1)) continue;
                num1 = num2;
                index1 = index2;
            }
            var clipIndex = Mathf.Abs(index1 - 14);

            // Push some events to the audio thread in ALLT
            var player = AlltPlugin.Instance.Player;
            player.PushSetClipToPlay((nuint)clipIndex);
            player.PushSetPitchMultiplier(__instance.currentnotesound.pitch);
            player.PushPlay();

            // Don't play the note sound in-game, since our mod does it
            __instance.currentnotesound.Stop();
        }

        [HarmonyPatch(typeof(GameController), nameof(GameController.stopNote))]
        [HarmonyPostfix]
        static void StopNote_Post()
        {
            AlltPlugin.Instance.Player.PushStop();
        }

        [HarmonyPatch(typeof(GameController), nameof(GameController.Update))]
        [HarmonyPostfix]
        static void Update_Post(GameController __instance)
        {
            var player = AlltPlugin.Instance.Player;

            // `__instance.currentnotesound` can be null before the game has had a chance to Do Stuff
            if (__instance.currentnotesound != null)
            {
                player.PushSetPitchMultiplier(__instance.currentnotesound.pitch);
            }

            if (!__instance.noteplaying)
            {
                player.PushFadeOut();
            }
        }

        internal static void OnSceneChanged(Scene oldScene, Scene newScene)
        {
            AlltPlugin.Instance.Player.StopPlayback();
        }
    }
}
