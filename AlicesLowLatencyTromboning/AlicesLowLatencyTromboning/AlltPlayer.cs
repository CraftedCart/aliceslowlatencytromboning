using System;
using System.Runtime.InteropServices;

namespace AlicesLowLatencyTromboning
{
    /// <summary>
    /// Wrapper around the <c>Player</c> type in Rust
    /// </summary>
    internal class AlltPlayer : IDisposable
    {
        private readonly IntPtr _inner;

        [DllImport("allt_playback.dll")]
        private static extern IntPtr allt_player_new();

        [DllImport("allt_playback.dll", EntryPoint = "allt_player_load", CharSet = CharSet.Ansi)]
        private static extern IntPtr allt_player_load_char(string configDir);

        [DllImport("allt_playback.dll", EntryPoint = "allt_player_load", CharSet = CharSet.Unicode)]
        private static extern IntPtr allt_player_load_wchar(string configDir);

        [DllImport("allt_playback.dll")]
        private static extern IntPtr allt_player_tick(IntPtr player);

        [DllImport("allt_playback.dll")]
        private static extern bool allt_player_start_playback(IntPtr player);

        [DllImport("allt_playback.dll")]
        private static extern void allt_player_stop_playback(IntPtr player);

        [DllImport("allt_playback.dll")]
        private static extern void allt_player_drop(IntPtr player);

        [DllImport("allt_playback.dll")]
        private static extern bool allt_player_push_play(IntPtr player);

        [DllImport("allt_playback.dll")]
        private static extern bool allt_player_push_stop(IntPtr player);

        [DllImport("allt_playback.dll")]
        private static extern bool allt_player_push_set_pitch_multiplier(IntPtr player, float multiplier);

        [DllImport("allt_playback.dll")]
        private static extern bool allt_player_push_fade_out(IntPtr player);

        [DllImport("allt_playback.dll")]
        private static extern bool allt_player_push_set_num_trombone_clips(IntPtr player, nuint num);

        [DllImport("allt_playback.dll")]
        private static extern bool allt_player_push_set_trombone_clip_samples(
            IntPtr player,
            nuint index,
            IntPtr samples,
            nuint numSamples,
            nuint channels
        );

        [DllImport("allt_playback.dll")]
        private static extern bool allt_player_push_set_clip_to_play(IntPtr player, nuint index);

        [DllImport("allt_playback.dll")]
        private static extern bool allt_player_open_config_ui(IntPtr player);

        internal AlltPlayer()
        {
            _inner = allt_player_new();
        }

        private AlltPlayer(string configDir)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                _inner = allt_player_load_wchar(configDir);
            }
            else
            {
                _inner = allt_player_load_char(configDir);
            }
        }

        internal static AlltPlayer Load()
        {
            return new AlltPlayer(BepInEx.Paths.ConfigPath);
        }

        ~AlltPlayer()
        {
            ReleaseUnmanagedResources();
        }

        private void ReleaseUnmanagedResources()
        {
            allt_player_drop(_inner);
        }

        public void Dispose()
        {
            ReleaseUnmanagedResources();
            GC.SuppressFinalize(this);
        }

        internal void Tick()
        {
            allt_player_tick(_inner);
        }

        internal void StartPlayback()
        {
            if (!allt_player_start_playback(_inner))
            {
                AlltPlugin.Instance.Logger.LogError("Failed to start playback");
            }
        }

        internal void StopPlayback()
        {
            allt_player_stop_playback(_inner);
        }

        internal void PushPlay()
        {
            if (!allt_player_push_play(_inner))
            {
                // AlltPlugin.Instance.Logger.LogError("Error pushing play event");
            }
        }

        internal void PushStop()
        {
            if (!allt_player_push_stop(_inner))
            {
                // AlltPlugin.Instance.Logger.LogError("Error pushing stop event");
            }
        }

        internal void PushSetPitchMultiplier(float multiplier)
        {
            if (!allt_player_push_set_pitch_multiplier(_inner, multiplier))
            {
                // AlltPlugin.Instance.Logger.LogError("Error pushing set pitch multiplier event");
            }
        }

        internal void PushFadeOut()
        {
            if (!allt_player_push_fade_out(_inner))
            {
                // AlltPlugin.Instance.Logger.LogError("Error pushing fade out event");
            }
        }

        internal void PushSetNumTromboneClips(nuint num)
        {
            if (!allt_player_push_set_num_trombone_clips(_inner, num))
            {
                AlltPlugin.Instance.Logger.LogError("Error pushing set num trombone clips event");
            }
        }

        internal void PushSetTromboneClipSamples(nuint index, float[] samples, nuint channels)
        {
            unsafe
            {
                fixed (float* samplesPtr = samples)
                {
                    if (!allt_player_push_set_trombone_clip_samples(_inner, index, (IntPtr)samplesPtr, (nuint)samples.Length, channels))
                    {
                        AlltPlugin.Instance.Logger.LogError("Error pushing set trombone clip samples event");
                    }
                }
            }
        }

        internal void PushSetClipToPlay(nuint index)
        {
            if (!allt_player_push_set_clip_to_play(_inner, index))
            {
                // AlltPlugin.Instance.Logger.LogError("Error pushing set clip to play event");
            }
        }

        internal void OpenConfigUi()
        {
            if (!allt_player_open_config_ui(_inner))
            {
                AlltPlugin.Instance.Logger.LogError("Failed to open config UI");
            }
        }
    }
}
